FROM python:slim
RUN apt-get update && apt-get install python3-pip openssh-client -y 
RUN pip3 install ansible docker requests
RUN ansible-galaxy collection install community.docker
CMD ["/bin/bash"]

